---
layout: post
title:  "Trigger jenkins curl"
date:   2020-11-04 12:51:10 +0300
slug: jenkins-trigger
categories: en CICD Jenkins
locale: en_US
lang: en
summary: "how to run Jenkins job using curl command remotely"
---

First of all you must have:
* Trigget plugin
* Jenkins User who has permissions:
  - Overall - Read
  - Job - Build
  - Job - Read
  - Job - Workspace
* TOKEN for Jenkins User


Create permissions for Jenkins User 
  1. Click on Manage Jenkins
  2. Click on Configure Global Security
  3. Assuming you’re using matrix-based security: add Jenkins User (curluser) to the list and check off the boxes for the necessary permissions
  4. Click Save

For example:

```bash
curl -u ${JENKINS_USER}:${JENKINS_TOKEN} -fsSL 'https://jenkins.server.org/job/Test/job/superjob/buildWithParameters?token=ieQueef5Oarum7oabire&BRANCH=test'
```

- `JENKINS_USER` - `curluser`
- `JENKINS_TOKEN` - `Omaegh1zohB8ieNahl4eieQueef5Oarum7oabire`

### Links
- [Jenkins][Jenkins]{:rel="nofollow" target="_blank"}

[Jenkins]: https://jenkins.io/


https://humanwhocodes.com/blog/2015/10/triggering-jenkins-builds-by-url/
https://serverfault.com/questions/888176/how-to-trigger-jenkins-job-via-curl-command-remotely#888248
