---
layout: post
title:  "USB Bootable"
date:   2019-11-11 13:42:00 +0300
slug: usb-bootable
categories: ru OS
locale: ru_RU
lang: ru
summary: "Создание загрузочной флешки"
---

### Создание загрузочной флешки

#### В Ubuntu

Сначала проверяем при помощи `fdisk` какое устройство нам нужно:
```bash
$ fdisk -l
```

Лучше всего работает практика создания через `dd`:
```bash
$ dd if=ubuntu-17.10-desktop-amd64.iso  of=/dev/sdb bs=64M status=progress 
```

#### В Windows

Для винды нужна программа `win32 disk imager`

#### Ссылки

* [Bootable USB](https://www.linuxtechi.com/create-bootable-usb-disk-dvd-ubuntu-linux-mint/){:rel="nofollow" target="_blank"}
* [win32 disk imager](https://sourceforge.net/projects/win32diskimager/){:rel="nofollow" target="_blank"}
