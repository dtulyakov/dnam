---
layout: post
title:  "Dockerfile"
date:   2019-11-12 10:22:00 +0300
slug: dockerfile
categories: ru containers
locale: ru_RU
lang: ru
summary: "Основные параметр Dockerfile"
---


### Структура dockerfile

* `FROM` - Образ на осонове которого происходит наследование
* `ARG` - Параметры которые живут **только в момент сборки**
* `ENV` - Параметры которые живут и после сборки
* `LABEL` - Параметры для ярлыков - время сборки, хеш коммита, ссылка настраницу проекта/докерфайла и т.д. 
* `RUN` - Каждый RUN отдельный слой, по максиуму надо стараться групировать их по логическим параметрам
* `ADD` - Добавляет файлы в каталог (архивы распаковывает, так же  можно  указывать URL)
* `COPY` - В отличии от ADD только копирует файлы

### Пример добавлеия репозитория
```bash
RUN set -x \
  && apt-get update -q \
  && apt-get install -q -y \
     dirmngr \
     gnupg2 \
  && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
  && echo "deb http://packages.ros.org/ros/ubuntu bionic main" > /etc/apt/sources.list.d/ros1-latest.list \
  && apt purge -y dirmngr \
     gnupg2 \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*
```

### Пример multi-stage сборки

```shell
FROM golang:1.7.3 AS builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN set -x \
  && go get -d -v golang.org/x/net/html
COPY app.go    .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN set -x \
  && apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"]
```

### Пример multi-stage копирование из внешнего образа
```shell
...
COPY --from=nginx:latest /etc/nginx/nginx.conf /nginx.conf
...
```

---

*Денис Валентинович*
