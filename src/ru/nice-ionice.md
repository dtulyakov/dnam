---
layout: post
title:  "Nice IOnice"
date:   2019-07-09 15:32:00 +0300
slug: nice-ionice
categories: ru OS
locale: ru_RU
lang: ru
summary: "Управление нагрузкой при запуске приложения"
---

##### Nice

* `-n` приоритет (FreeBSD от -20 наивысший до 20 низший приритет; в Linux -20 наивысший 19 низший приритет)
* `--adjustment=смещение` Установить приоритет nice, равный сумме текущего приоритета nice и указанного числа «смещение». Если этот аргумент не указан, будет использовано число 10.

```bash

nice -n -20 cp /root/.ssh/id.rsa /tmp
```
##### IONice

* `-c` Классы 1 - реалтайм; 2 - best-effort; 3 - idle
* `-n` для 1 и 2 классов допустимые значения от 0 до 7
* `-p` Идентификатор процесса (применяется для изменения уже запущенного процесса)


```bash

ionice c2 -n7 rsync /root/.ssh/id.rsa /tmp
```

Так же можно совмещать оба варианта

```bash
nice -n 20 \
     ionice -c2 -n7 docker exec -t Mysql sh -c "mysqldump \
     -udebian-sys-maint \
     -password \
     --single-transaction \
     --routines \
     --triggers \
     --default-character-set=utf8 \
     --events mydatabase \
     --ignore-table=mydatabase.snmp_data \
     --ignore-table=mydatabase.Log \
     --ignore-table=mydatabase.LogRequest" > /tmp/mysqtabase.sql'
```


---

*Денис Валентинович*
